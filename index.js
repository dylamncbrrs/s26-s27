/*GOAL: Createa a server using 
 'Express.js'*/

/*1. Use the require() directive to 
 acquire the express library and it's
  utilities*/
	const express = require('express');

/*2. Createa a server using only express*/
	const server = express();
	/*express() => will allow us to create
	an express application.*/

/*I dentify a port/address that will server/host
this newly created connection/app*/
	const address = 3000;

/*4. Bind the application to the desired designated
port using the listen(). Create a mothod that will
display a response that the connection has been
established.*/
	server.listen(address, () =>{
		console.log(`Server is running on port: ${address}`)
	});
